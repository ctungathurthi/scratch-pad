/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.carzonrent.tckb.scratch.hashmap;

/**
 * A Bi-directional mapnode. This node maintain both forward and backward
 * reference to the next node and previous node respectively.
 * <p>
 * @author tckb
 */
public class PrivateMapBiNode {

    /**
     * Immutable integer key.
     */
    public final int key;
    /**
     * Immutable object value.
     */
    public final Object Value;
    /**
     * (Mutable) Reference to next node.
     */
    public PrivateMapBiNode next;
    /**
     * (Mutable) Reference to previous node.
     */
    public PrivateMapBiNode previous;

    public PrivateMapBiNode(int key, Object Value) {
        this.key = key;
        this.Value = Value;
    }

}
