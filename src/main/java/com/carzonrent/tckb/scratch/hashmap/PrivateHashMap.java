/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.carzonrent.tckb.scratch.hashmap;

/**
 * An abstract class defining hashmap.
 * <p>
 * <p>
 * @author tckb
 * @param <K> Key object
 * @param <V> Value object
 */
public abstract class PrivateHashMap<K, V> implements PrivateMap<K, V> {

    private final int n = 1;
    protected final int TABLE_SIZE = (int) Math.pow(2, n) + 1;
}
