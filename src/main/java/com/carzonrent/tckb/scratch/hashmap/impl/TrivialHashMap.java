/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.carzonrent.tckb.scratch.hashmap.impl;

import com.carzonrent.tckb.scratch.hashmap.PrivateHashMap;
import com.carzonrent.tckb.scratch.hashmap.PrivateMapUniNode;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An implmentation of PrivateHashMap
 *
 * @author tckb
 */
public class TrivialHashMap<K, V> extends PrivateHashMap<K, V> {

    private final PrivateMapUniNode[] hashTable;
    private final Logger logger = Logger.getLogger(getClass().getName());
    private int nrElements;

    public TrivialHashMap() {
        logger.info("Initializing hashmap logger...");
        hashTable = new PrivateMapUniNode[super.TABLE_SIZE];
        logger.log(Level.INFO, "hashTable.size: {0}", hashTable.length);
        nrElements = 0;
    }

    @Override
    public V get(K key) {

        if (key == null) {
            throw new IllegalArgumentException("Key must not be null");
        }
        V returnValue = null;

        int index = getHashTableIndex(key.hashCode());

        logger.log(Level.INFO, "Getting key: {0} -> {1}", new Object[]{key.hashCode(), index});

        PrivateMapUniNode<K, V> thisNode = hashTable[index];

        if (thisNode != null) {

            if (thisNode.next != null) {

                while (thisNode.next != null) {
                    logger.log(Level.INFO, "thisNode.key: {0} value: {1}", new Object[]{thisNode.hash, thisNode.value});
                    if (thisNode.hash == key.hashCode() && key.equals(thisNode.key)) {
                        returnValue = thisNode.value;
                    }
                    thisNode = thisNode.next;
                }
            } else {
                if (thisNode.hash == key.hashCode() && key.equals(thisNode.key)) {
                    returnValue = thisNode.value;
                }
            }
        }
        return returnValue;
    }

    @Override
    public V put(K key, V value) {

        if (key == null || value == null) {
            throw new IllegalArgumentException("<K,V> pair must not be null");
        }

        PrivateMapUniNode<K, V> newNode = new PrivateMapUniNode(key.hashCode(), key, value);
        boolean betweenNodeReplaced = false;
        boolean replaceFirstNode = false;

        int index = getHashTableIndex(key.hashCode());

        logger.log(Level.INFO, "Putting key: {0} -> {1}", new Object[]{key.hashCode(), index});

        PrivateMapUniNode thisNode = hashTable[index];

        if (thisNode != null) {

            if (thisNode.next == null && thisNode.hash == key.hashCode() && thisNode.key.equals(key)) {
                replaceFirstNode = true;
            }
            while (thisNode.next != null) {
                logger.log(Level.INFO, "thisNode.key: {0} value: {1}", new Object[]{thisNode.hash, thisNode.value});

                // replacing a node in between
                if (thisNode.next.hash == key.hashCode() && thisNode.next.key.equals(key)) {
                    PrivateMapUniNode oldNode = thisNode.next;
                    logger.log(Level.INFO, "oldNode.key: {0} value: {1}", new Object[]{oldNode.hash, oldNode.value});

                    thisNode.next = newNode;
                    newNode.next = oldNode.next;
                    // delete the reference, hope the garbage collector will take care
                    oldNode = null;
                    betweenNodeReplaced = true;
                    break;

                } else {
                    thisNode = thisNode.next;
                }
            }
            if (replaceFirstNode) {
                PrivateMapUniNode oldNode = thisNode;
                logger.log(Level.INFO, "firstNode.key: {0} value: {1}", new Object[]{oldNode.hash, oldNode.value});
                hashTable[index] = newNode;
                // delete the reference, hope the garbage collector will take care
                oldNode = null;
            }
            if (!betweenNodeReplaced && !replaceFirstNode) {
                thisNode.next = newNode;
                nrElements++;
            }

        } else {
            logger.log(Level.INFO, "Node.key: {0} value: {1}", new Object[]{newNode.hash, newNode.value});
            hashTable[index] = newNode;
            nrElements++;
        }

        return newNode.value;
    }

    @Override
    public V remove(K key) {
        if (key == null) {
            throw new IllegalArgumentException("<K,V> pair must not be null");
        }

        int index = getHashTableIndex(key.hashCode());
        PrivateMapUniNode<K, V> thisNode = hashTable[index];
        if (thisNode != null) {

            V returnValue = null;

            if (thisNode.next != null) {
                // rewiring of the nodes
                logger.info("iterating through the list");

                while (thisNode.next != null) {
                    if (thisNode.next.hash == key.hashCode() && key.equals(thisNode.next.key)) {
                        // get the deleted node
                        PrivateMapUniNode<K, V> deletedNode = thisNode.next;
                        // rewire the node
                        thisNode.next = deletedNode.next;
                        returnValue = deletedNode.value;
                        // remove the reference of the next node of the one deleted
                        deletedNode.next = null;

                    } else {
                        thisNode = thisNode.next;
                    }
                }
            } // there are no more tail elements
            else {
                if (thisNode.hash == key.hashCode()) {
                    returnValue = thisNode.value;
                    // hope garbage collector would do its job
                    thisNode = null;
                    hashTable[index] = null;
                }
            }

            if (returnValue != null) {
                nrElements--;
            }

            return returnValue;
        }
        return null;
    }

    @Override
    public void clear() {
        for (int i = 0; i < hashTable.length; i++) {
            hashTable[i] = null;
        }
        nrElements = 0;
        // request garbage collection!
        System.gc();
    }

    @Override
    public int size() {
        return nrElements;
    }

    @Override
    public boolean isEmpty() {
        return nrElements == 0;
    }

    /**
     * Returns the index of the hash table
     * <p>
     *
     * @param hashValue <p>
     * @return
     */
    private int getHashTableIndex(int hashValue) {

        int ix = hashValue % TABLE_SIZE;
        return Math.abs(ix);
    }

    @Override
    public String toMapString() {
        StringBuilder mapString = new StringBuilder();
        mapString.append("\n")
                .append("#Map nodes: ").append(this.size())
                .append("\n");
        for (int i = 0; i < TABLE_SIZE; i++) {
            PrivateMapUniNode nodeAtI = hashTable[i];
            if (nodeAtI != null) {
                mapString
                        .append("(index=").append(i).append(")\n")
                        .append("{hash=").append(nodeAtI.hash).append(",key=").append(nodeAtI.key).append(",value=").append(nodeAtI.value).append("}");
                PrivateMapUniNode thisNode = nodeAtI;
                while (thisNode.next != null) {
                    thisNode = thisNode.next;
                    mapString.append("-->{key=").append(thisNode.hash).append(",key=").append(thisNode.key).append(",value=").append(thisNode.value).append("}");
                }
                mapString.append("-->X\n");
            }
        }
        mapString.trimToSize();
        return mapString.toString();
    }
}
