/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.carzonrent.tckb.scratch.hashmap;

/**
 * A uni-directional Map node, this node maintains a forward reference to the
 * next node.
 * <p>
 * <p>
 * @author tckb
 * @param <K>
 * @param <V>
 */
public class PrivateMapUniNode<K, V> {

    /**
     * Immutable integer hash.
     */
    public final int hash;

    /**
     * Immutable key.
     */
    public final K key;

    /**
     * Immutable value.
     */
    public final V value;
    /**
     * (Mutable) Reference to the next node.
     */
    public PrivateMapUniNode next;

    /**
     * Create a Uni-directional map node with hash and value.
     * <p>
     * @param hash
     * @param key
     * @param Value
     */
    public PrivateMapUniNode(int hash, K key, V Value) {
        this.hash = hash;
        this.value = Value;
        this.key = key;
    }

}
