/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.carzonrent.tckb.scratch.hashmap;

/**
 * A non-thread safe map defining basic operations of map
 * <p>
 * @author tckb
 * @param <K> Key object
 * @param <V> Value object
 */
public interface PrivateMap<K, V> {

    /**
     * Returns the value associated with the key
     * <p>
     * @param key a non-null associated key.
     * <p>
     * @return returns value associated with the key. returns null if key is not
     *         present in the map
     */
    V get(K key);

    /**
     * Inserts the K,V pair into the map;
     * <p>
     * Must solve, issues of <code>hash-collision</code>,
     * <code>index-collision</code>
     * <p>
     * <p>
     * @param key   associated key.
     * @param value associated value.
     * <p>
     * @return inserted value
     */
    V put(K key, V value);

    /**
     * Remove the key from the map
     * <p>
     * @param key <p>
     * @return value associated with the key
     */
    V remove(K key);

    /**
     * Clears all elements from the map
     * <p>
     */
    void clear();

    /**
     * Returns the size of the map
     * <p>
     * @return size of the map
     */
    int size();

    /**
     * Checks if the map is empty.
     * <p>
     * <p>
     * @return true -if empty, false - otherwise
     */
    boolean isEmpty();

    /**
     * Returns the string representation of the map; analogies to
     * <code>Object.toString()</code>
     * <p>
     * @return string representation of the map
     */
    String toMapString();

}
