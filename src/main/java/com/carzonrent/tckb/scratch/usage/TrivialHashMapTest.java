package com.carzonrent.tckb.scratch.usage;

import com.carzonrent.tckb.scratch.hashmap.PrivateHashMap;
import com.carzonrent.tckb.scratch.hashmap.impl.TrivialHashMap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author tckb
 *         Created on 31/03/16.
 */
public class TrivialHashMapTest {

    private PrivateHashMap<Friend, Integer> myFriendsSalaries;
    private Friend mike_Google, sally_Yahoo, sally_free, jacob_flip, sri_micro;


    @Before
    public void setUp() throws Exception {
        mike_Google = new Friend("123", "Mike", "Google");
        sally_Yahoo = new Friend("234", "sally", "Yahoo");
        sally_free = new Friend("234", "sally", "Freelancing");
        jacob_flip = new Friend("456", "jacob", "Flipkart");
        sri_micro = new Friend("678", "sri", "Microsoft");

        myFriendsSalaries = new TrivialHashMap<>();
    }

    @Test
    public void testBeforeAdding() {
        Assert.assertEquals(myFriendsSalaries.size(), 0);
        Assert.assertEquals(myFriendsSalaries.isEmpty(), true);

    }

    @Test
    public void testAfterAdding() {
        addEntries();

        Assert.assertEquals(myFriendsSalaries.size(), 5);
        Assert.assertEquals(myFriendsSalaries.isEmpty(), false);

    }

    @Test
    public void testEntries() {
        addEntries();
        Assert.assertEquals(myFriendsSalaries.size(), 5);
        Assert.assertEquals((int) myFriendsSalaries.get(jacob_flip), 15);
        Assert.assertEquals((myFriendsSalaries.get(sally_Yahoo) + myFriendsSalaries.get(sally_free)), 30);

    }


    @Test
    public void testSallyLostJob() {
        addEntries();
        int salartBefore = myFriendsSalaries.get(sally_free);
        int countBefore = myFriendsSalaries.size();
        int salaryAfter = myFriendsSalaries.remove(sally_free);

        Assert.assertEquals(salaryAfter, salartBefore);
        Assert.assertEquals(myFriendsSalaries.size(), countBefore - 1);
        Assert.assertEquals((int) myFriendsSalaries.get(sally_Yahoo), 10);
        Assert.assertNull(myFriendsSalaries.get(sally_free));
    }

    @Test
    public void testClear() {
        addEntries();

        Assert.assertEquals(myFriendsSalaries.size(), 5);
        myFriendsSalaries.clear();
        Assert.assertEquals(myFriendsSalaries.size(), 0);
        Assert.assertEquals(myFriendsSalaries.isEmpty(), true);

    }


    @Test
    public void testGotHerJob() {
        addEntries();

        Assert.assertEquals(myFriendsSalaries.size(), 5);
        myFriendsSalaries.clear();

        myFriendsSalaries.put(sally_free, 10);
        Assert.assertEquals((int) myFriendsSalaries.get(sally_free), 10);

        myFriendsSalaries.put(sally_free, 50);
        Assert.assertEquals((int) myFriendsSalaries.get(sally_free), 50);

    }


    @Test(expected = IllegalArgumentException.class)
    public void testThrowsException() {
        addEntries();
        Assert.assertEquals(myFriendsSalaries.size(), 5);
        myFriendsSalaries.clear();
        myFriendsSalaries.put(sally_free, null);
    }


    private void addEntries() {
        // push them to hashmap
        myFriendsSalaries.put(sally_Yahoo, 10);
        myFriendsSalaries.put(mike_Google, 11);
        myFriendsSalaries.put(sally_free, 20);
        myFriendsSalaries.put(jacob_flip, 15);
        myFriendsSalaries.put(sri_micro, 50);
    }


}