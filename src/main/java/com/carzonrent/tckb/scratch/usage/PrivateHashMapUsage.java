/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.carzonrent.tckb.scratch.usage;

import com.carzonrent.tckb.scratch.hashmap.PrivateHashMap;
import com.carzonrent.tckb.scratch.hashmap.impl.TrivialHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tckb
 */
public class PrivateHashMapUsage {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Logger.getLogger(TrivialHashMap.class.getName()).setLevel(Level.OFF);

        PrivateHashMap<Friend, Integer> myFriendsSalaries = new TrivialHashMap<>();

        System.out.println("HashMap size: " + myFriendsSalaries.size());
        System.out.println("HashMap isEmpty: " + myFriendsSalaries.isEmpty());

        // create elemets
        Friend mike_Google = new Friend("123", "Mike", "Google");
        Friend sally_Yahoo = new Friend("234", "sally", "Yahoo");
        Friend sally_free = new Friend("234", "sally", "Freelancing");
        Friend jacob_flip = new Friend("456", "jacob", "Flipkart");
        Friend sri_micro = new Friend("678", "sri", "Microsoft");

        System.out.println("Pushing into hashmap");

        // push them to hashmap
        myFriendsSalaries.put(sally_Yahoo, 10);
        myFriendsSalaries.put(mike_Google, 11);
        myFriendsSalaries.put(sally_free, 20);
        myFriendsSalaries.put(jacob_flip, 15);
        myFriendsSalaries.put(sri_micro, 50);

        System.out.println("HashMap size: " + myFriendsSalaries.size());
        System.out.println("HashMap isEmpty: " + myFriendsSalaries.isEmpty());

        // retrieve stuff from map
        System.out.println(">>Jacobs earning: " + myFriendsSalaries.get(jacob_flip) + "k");
        System.out.println(">>Sally's total earnings: " + (myFriendsSalaries.get(sally_Yahoo) + myFriendsSalaries.get(sally_free)) + "k");

        // remove stuff from hashmap
        int salary = myFriendsSalaries.remove(sally_free);
        System.out.println(">>Sally lost her job of income: " + salary + "k");

        System.out.println("HashMap size: " + myFriendsSalaries.size());
        System.out.println("HashMap isEmpty: " + myFriendsSalaries.isEmpty());

        System.out.println(">>Sally's current earnings: " + myFriendsSalaries.get(sally_Yahoo));
        System.out.println(">>Did sally get her old job: " + (myFriendsSalaries.get(sally_free) != null));

        System.out.println(">>All my friends salary: " + myFriendsSalaries.toMapString());

        System.out.println("clearing");

        myFriendsSalaries.clear();

        System.out.println("HashMap size: " + myFriendsSalaries.size());
        System.out.println("HashMap isEmpty: " + myFriendsSalaries.isEmpty());

        myFriendsSalaries.put(sally_free, 10);
        System.out.println(">>Sally's current earnings: " + myFriendsSalaries.get(sally_free) + "k");
        System.out.println("HashMap size: " + myFriendsSalaries.size());

        System.out.println(">>All my friends salary: " + myFriendsSalaries.toMapString());

        System.out.println(">>Sally's has got good freelancing job");
        myFriendsSalaries.put(sally_free, 55);

        System.out.println("HashMap size: " + myFriendsSalaries.size());
        System.out.println(">>Sally's total earnings after new freelancing job: " + myFriendsSalaries.get(sally_free) + "k");
        System.out.println(">>All my friends salary: " + myFriendsSalaries.toMapString());

        System.out.println("There should be an exception now.");
        // all the calls below are invalid
//        myFriendsSalaries.put(null, null);
        myFriendsSalaries.put(sally_free, null);
//        myFriendsSalaries.put(null, 10);
//        myFriendsSalaries.get(null);
    }

}
