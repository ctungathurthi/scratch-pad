/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.carzonrent.tckb.scratch.usage;

import java.util.Objects;

/**
 *
 * @author tckb
 */
public class Friend {

    private String phone;
    private String firstName;
    private String company;

    public Friend(String phone, String firstName, String company) {
        this.phone = phone;
        this.firstName = firstName;
        this.company = company;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public int hashCode() {

        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.phone);
        hash = 31 * hash + Objects.hashCode(this.firstName);
        hash = 31 * hash + Objects.hashCode(this.company);

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Friend other = (Friend) obj;
        if (!Objects.equals(this.phone, other.getPhone())) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.getFirstName())) {
            return false;
        }
        return Objects.equals(this.company, other.getCompany());
    }

    @Override
    public String toString() {
        return "Friend[" + "phone=" + phone + ", firstName=" + firstName + ", company=" + company + ']';
    }

}
